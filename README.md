# README #

### What is ComerUA? ###
ComerUA is an Android application prototype which uses open data from the University of Alicante. I made it with the help of Daniel Ruiz Franco, as the graphic designer, to take part of the first competition of applications of the UA (http://bit.do/32xY).

It shows information of all the restaurants in the campus, included their products and with votes and comments. It was made as a social network to show the real difference between restaurants: the quality.

This project is gonna be restart during the summer to develop a real app for the UA community.

Version 1.0

### Who do I talk to? ###

Carolina Prada Hernández (cphcarolina@gmail.com)