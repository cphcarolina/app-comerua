package cphcarolina.comerua;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cphcarolina.comerua.dao.UsuarioDAO;
import cphcarolina.comerua.fragments.ListadoCafeterias;


public class LoginActivity extends Activity {
    EditText editEmail, editContrasenia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(R.string.app_name);
        TextView titulo = (TextView) findViewById(R.id.textCafeteriaTitle);
        titulo.setText("Inicio de sesión");
        titulo.setTextColor(getResources().getColor(R.color.cabecera));

        SharedPreferences pref = getSharedPreferences("PreferenciasComerUA", Context.MODE_PRIVATE);
        boolean isLogged = pref.getBoolean("isLogged", false);

        if (isLogged) {
            Intent intent = new Intent(null, ListadoCafeterias.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        editEmail = (EditText) findViewById(R.id.editEmail);
        editContrasenia = (EditText) findViewById(R.id.editContrasenia);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        TextView lblRegistro = (TextView) findViewById(R.id.lblRegistro);
        TextView lblRecuperar = (TextView) findViewById(R.id.lblContrasenia);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UsuarioDAO user = new UsuarioDAO(LoginActivity.this);
                String email = String.valueOf(editEmail.getText());
                String contrasenia = String.valueOf(editContrasenia.getText());

                if (email == null || contrasenia == null) {
                    Toast.makeText(LoginActivity.this, "Rellene todos los campos para iniciar la sesión",
                            Toast.LENGTH_LONG).show();
                } else if (!user.login(email, contrasenia)) {
                    Toast.makeText(LoginActivity.this, "El nombre o la contraseña son erróneos, inténtelo de nuevo",
                            Toast.LENGTH_LONG).show();
                } else {
                    SharedPreferences pref = getSharedPreferences("PreferenciasComerUA", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean("isLogged", true);
                    editor.commit();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        lblRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });

        /**
         * TODO hacer la recuperación de contraseña, recordar hacer visible la label
         lblRecuperar.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
        Intent intent = new Intent(LoginActivity.this, RecoveryActivity.class);
        startActivity(intent);
        }
        });
         */
    }
}
