package cphcarolina.comerua.vo;

import java.util.Random;

/**
 * Created by Carolina on 04/03/2015.
 */
public class ProductoVO {
    private String nombre;
    private String descripcion;
    private String categoria;
    private float precio;
    private int votosDiario;
    private int votosGlobal;
    private float puntuacionDiario;
    private float puntuacionGlobal;


    public ProductoVO(String nombre, float precio) {
        Random r = new Random();
        this.nombre = nombre;
        this.categoria = "Sin Categoría";
        this.precio = precio;
        this.votosDiario = r.nextInt(20);
        this.votosGlobal = r.nextInt(80) + votosDiario;
        this.puntuacionDiario = (r.nextFloat() * (votosDiario*5)) - 1;
        this.puntuacionGlobal = (r.nextFloat() * (votosGlobal*5)) - 1;
        this.descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis nisl aliquam, sodales nisl sed, hendrerit arcu.";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getstrPrecio() { return precio +" €"; }

    public int getVotosDiario() {
        return votosDiario;
    }

    public void setVotosDiario(int votosDiario) {
        this.votosDiario = votosDiario;
    }

    public int getVotosGlobal() {
        return votosGlobal;
    }

    public void setVotosGlobal(int votosGlobal) {
        this.votosGlobal = votosGlobal;
    }

    public float getPuntuacionDiario() {
        return puntuacionDiario/votosDiario;
    }

    public void setPuntuacionDiario(float puntuacionDiario) {
        this.puntuacionDiario = puntuacionDiario;
    }

    public float getPuntuacionGlobal() {
        return puntuacionGlobal/votosGlobal;
    }

    public void setPuntuacionGlobal(float puntuacionGlobal) {
        this.puntuacionGlobal = puntuacionGlobal;
    }
}
