package cphcarolina.comerua.vo;

import java.util.ArrayList;
import java.util.Map;

import cphcarolina.comerua.bo.HorarioBO;

/**
 * Created by Carolina on 17/02/2015.
 */
public class CafeteriaVO {
    private final String TAG = "CafeteriaVO";
    private int indice;

    // Datos contacto
    private String nombre;
    private String responsable;
    private Map<String, Integer> telefonos;
    private Map<String, String> emails;
    private ArrayList<HorarioBO> horarios;
    private boolean abierto;

    // Datos instalaciones
    private int aforo;
    private int futbolin;
    private int billar;
    private int microondas;

    // Datos valoracion
    private int votosGlobal;
    private int votosDiario;
    private float puntuacionGlobal;
    private float puntuacionDiario;

    public CafeteriaVO() {
        abierto = false;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Map<String, Integer> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(Map<String, Integer> telefonos) {
        this.telefonos = telefonos;
    }

    public Map<String, String> getEmails() {
        return emails;
    }

    public void setEmails(Map<String, String> emails) {
        this.emails = emails;
    }

    public ArrayList<HorarioBO> getHorarios() {
        return horarios;
    }

    public void setHorarios(ArrayList<HorarioBO> horarios) {
        this.horarios = horarios;
    }

    public boolean isAbierto() {
        return abierto;
    }

    public void setAbierto(boolean abierto) {
        this.abierto = abierto;
    }

    public int getAforo() {
        return aforo;
    }

    public void setAforo(int aforo) {
        this.aforo = aforo;
    }

    public int getFutbolin() {
        return futbolin;
    }

    public void setFutbolin(int futbolin) {
        this.futbolin = futbolin;
    }

    public int getBillar() {
        return billar;
    }

    public void setBillar(int billar) {
        this.billar = billar;
    }

    public int getMicroondas() {
        return microondas;
    }

    public void setMicroondas(int microondas) {
        this.microondas = microondas;
    }

    public int getVotosGlobal() {
        return votosGlobal;
    }

    public void setVotosGlobal(int votosGlobal) {
        this.votosGlobal = votosGlobal;
    }

    public int getVotosDiario() {
        return votosDiario;
    }

    public void setVotosDiario(int votosDiario) {
        this.votosDiario = votosDiario;
    }

    public float getPuntuacionGlobal() {
        return puntuacionGlobal/votosGlobal;
    }

    public void setPuntuacionGlobal(float puntuacionGlobal) {
        this.puntuacionGlobal = puntuacionGlobal;
    }

    public float getPuntuacionDiario() {
        return puntuacionDiario/votosDiario;
    }

    public void setPuntuacionDiario(float puntuacionDiario) {
        this.puntuacionDiario = puntuacionDiario;
    }

    public void addVoto(float rating) {
        votosDiario++;
        votosGlobal++;
        puntuacionGlobal += rating;
        puntuacionDiario += rating;
    }
}
