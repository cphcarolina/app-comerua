package cphcarolina.comerua.vo;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cphcarolina.comerua.R;
import cphcarolina.comerua.fragments.ListadoProductos;

/**
 *
 * Created by Carolina on 11/04/2015.
 */
public class ButtonIcon extends View {
    private static final String TAG = "ButtonIcon";
    private RelativeLayout layout;
    private ImageButton btn;
    private TextView text;

    public ButtonIcon(Context context, int id, int img, String texto, int background){
        super(context);
        layout = (RelativeLayout) findViewById(id);

        text = (TextView) layout.getChildAt(1);
        text.setText(texto);

        btn = (ImageButton) layout.getChildAt(0);
        btn.setImageResource(img);
        btn.setBackgroundResource(background);
    }

    public ButtonIcon(Context context, int id, int img, String texto, int background, final String categoria) {
        super(context);
        RelativeLayout layout = (RelativeLayout) findViewById(id);

        TextView txt = (TextView) layout.getChildAt(1);
        txt.setText(texto);

        ImageButton btn = (ImageButton) layout.getChildAt(0);
        btn.setImageResource(img);
        btn.setBackgroundResource(background);


    }

    public void setOnClickListener () {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Log.d(TAG, "button " + categoria + " pressed");
                Bundle args = new Bundle();
                args.putInt(ARG_CAFETERIA_INDEX,indiceCafeteria);
                args.putString(ListadoProductos.ARG_CATEGORIA_NAME, categoria);

                Fragment listadoProductos = new ListadoProductos();
                listadoProductos.setArguments(args);
                fragmentMng.beginTransaction()
                        .replace(R.id.content_frame, listadoProductos)
                        .addToBackStack("listado"+categoria)
                        .commit();
                        */
            }
        });
    }
}
