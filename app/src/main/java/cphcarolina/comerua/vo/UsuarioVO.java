package cphcarolina.comerua.vo;

/**
 * Created by Carolina on 08/03/2015.
 */
public class UsuarioVO {
    private String nombre;
    private String email;
    private String contrasenia;

    public UsuarioVO() {
        nombre = "Carolina";
        email = "cphcarolina@gmail.com";
        contrasenia = "qwe";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
}
