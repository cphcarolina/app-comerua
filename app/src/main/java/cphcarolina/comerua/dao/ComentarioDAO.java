package cphcarolina.comerua.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cphcarolina.comerua.listHelpers.RowCafeteria;
import cphcarolina.comerua.listHelpers.RowComentario;

/**
 * Created by Carolina on 03/03/2015.
 */
public class ComentarioDAO extends DataManager {
    private final String TAG = "ComentarioDAO";
    private final String comentariosFile = "comentarios.json";

    public ComentarioDAO(Context context) {
        super(context);
    }

    public ArrayList<RowComentario> getPrimeros(int index) {
        ArrayList<RowComentario> comentarios = getComentarios(index);

        while (comentarios.size()>3) {
            comentarios.remove(3);
        }

        return comentarios;
    }

    public ArrayList<RowComentario> getComentarios(int index) {
        ArrayList<RowComentario> comentarios = new ArrayList<>();
        RowComentario comentario;
        boolean encontrada = false;
        JSONArray arrayCompleto, array;
        JSONObject cafeteria = null, object;
        String nombre, fecha, texto;

        try {
            arrayCompleto = new JSONArray(loadData(comentariosFile));
            for (int i = 0; i < arrayCompleto.length(); i++) {
                cafeteria = arrayCompleto.getJSONObject(i);
                if (cafeteria.getInt("id") == index) {
                    encontrada = true;
                    break;
                }
            }

            if (encontrada) {
                array = cafeteria.getJSONArray("comentarios");
                for (int i = 0; i < array.length(); i++) {
                    object = array.getJSONObject(i);

                    try {
                        nombre = object.getString("nombre");
                    } catch (JSONException e) {
                        Log.e(TAG, "at getComentario problem with the user name.");
                        nombre = " - ";
                    }

                    try {
                        texto = object.getString("texto");
                    } catch (JSONException e) {
                        Log.e(TAG, "at getComentario problem with the comment content.");
                        texto = "Comentario no cargado";
                    }

                    try {
                        fecha = object.getString("fecha");
                    } catch (JSONException e) {
                        Log.e(TAG, "at getComentario problem with the comment date.");
                        fecha = "";
                    }

                    comentario = new RowComentario(nombre, fecha, texto);
                    comentarios.add(comentario);
                }
            } else {
                Log.v(TAG, "at getComentarios it doesn't found the index " + index);
            }

        } catch (JSONException e) {
            Log.e(TAG, "at getComentarios");
            e.printStackTrace();
        }

        return comentarios;
    }
}
