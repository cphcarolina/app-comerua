package cphcarolina.comerua.dao;

import android.content.Context;

import cphcarolina.comerua.vo.UsuarioVO;

/**
 * TODO
 * Created by Carolina on 08/03/2015.
 */
public class UsuarioDAO extends DataManager {
    private final String TAG = "UsuarioDAO";
    private UsuarioVO user;

    public UsuarioDAO(Context context) {
        super(context);
        user = new UsuarioVO();
    }

    public boolean login(String email, String contrasenia) {
        boolean correcto = false;

        if (email.equals(user.getEmail()) && contrasenia.equals(user.getContrasenia()))
            correcto = true;

        return correcto;
    }

    public boolean signup(String nombre, String email, String contrasenia) {
        boolean correcto = false;

        if (!nombre.isEmpty() && !email.isEmpty() && !contrasenia.isEmpty()) {
            user.setNombre(nombre);
            user.setEmail(email);
            user.setContrasenia(contrasenia);
            correcto = true;
        }

        return correcto;
    }

    @Override
    public String loadData(String resource) {
        return super.loadData(resource);
    }
}
