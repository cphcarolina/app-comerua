package cphcarolina.comerua.dao;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Carolina on 03/03/2015.
 */
public abstract class DataManager {
    protected Context context;
    private String TAG = "Abstract DataManager";

    public DataManager(Context context) {
        this.context = context;
    }

    public boolean busquedaBinaria(String dato) {
        String[] missed = {"A", "ANTE", "BAJO", "CABE", "CON", "CONTRA", "DE", "DESDE", "EN",
                "ENTRE", "gr", "HACIA", "HASTA", "ml", "O", "PARA", "POR", "SEGÚN", "SIN", "SO", "SOBRE", "TRAS", "Y"};
        int n = missed.length;
        int centro, inf = 0, sup = n - 1;

        while (inf <= sup) {
            centro = (sup + inf) / 2;
            if (missed[centro].equals(dato)) {
                return true;
            } else if (missed[centro].compareTo(dato) > 0) {
                sup = centro - 1;
            } else {
                inf = centro + 1;
            }
        }
        return false;
    }

    public String capitalisation(String oldString) {
        String newString = "";
        String[] words = oldString.split(" ");

        for (int i = 0; i < words.length; i++) {
            if (busquedaBinaria(words[i]) && i != 0) {
                newString += words[i].toLowerCase();
            } else if(words[i].equals("I") || words[i].equals("II") || words[i].equals("III")) {
                newString += words[i];
            } else {
                newString += words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
            }

            if (i < words.length - 1) newString += " ";
        }
        return newString;
    }

    public String loadData(String resource) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(resource);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e(TAG, "at loadComentarios");
            e.printStackTrace();
        }
        return json;
    }

}
