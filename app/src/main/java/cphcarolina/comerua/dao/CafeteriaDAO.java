package cphcarolina.comerua.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import cphcarolina.comerua.bo.HorarioBO;
import cphcarolina.comerua.listHelpers.RowCafeteria;
import cphcarolina.comerua.vo.CafeteriaVO;

/**
 * Cafe Data Access Object
 * Created by Carolina on 03/03/2015.
 */
public class CafeteriaDAO extends DataManager {
    private final String TAG = "CafeteriaDAO";
    private final String cafeteriasFile = "cafeterias.json";
    private final String puntuacionesFile = "cafeterias-puntuacion.json";

    public CafeteriaDAO(Context context) {
        super(context);
    }

    public ArrayList<RowCafeteria> listCafeteriasAZ() {
        ArrayList<RowCafeteria> result = listCafeterias();
        Collections.sort(result, new NameComparator());
        return result;
    }

    public String[] getListado() {
        ArrayList<String> cafeterias = new ArrayList<>();
        JSONObject object;

        try {
            JSONArray array = new JSONArray(loadData(cafeteriasFile));

            for(int i=0; i<array.length(); i++) {
                object = array.getJSONObject(i);
                try {
                    cafeterias.add(capitalisation(object.getString("nombre")));
                } catch (JSONException e) {
                    Log.e(TAG, "at listCafeterias problem with the name.");
                    cafeterias.add("Cafetería no cargada");
                }
            }

            Collections.sort(cafeterias);

        } catch (JSONException e) {
            Log.e(TAG, "at getListado problem open the file "+cafeteriasFile);
            e.printStackTrace();
        }
        String[] array = new String[cafeterias.size()];
        for(int i=0;i<cafeterias.size(); i++) {
            array[i] = cafeterias.get(i);
        }

        return array;
    }

    public ArrayList<RowCafeteria> listCafeteriasDistancia() {
        // TODO
        return null;
    }

    public ArrayList<RowCafeteria> listCafeteriasValoracion() {
        ArrayList<RowCafeteria> result = listCafeterias();
        Collections.sort(result, new ValuationComparator());
        return result;
    }

    private ArrayList<RowCafeteria> listCafeterias() {
        ArrayList<RowCafeteria> result = new ArrayList<>();
        HashMap<String,String> nombres = new HashMap<>();
        RowCafeteria row;
        JSONObject object;
        String id, nombre;
        Float global;
        int votos;

        try {
            JSONArray arrayCafeteria = new JSONArray(loadData(cafeteriasFile));
            JSONArray arrayPuntuacion = new JSONArray(loadData(puntuacionesFile));
            for (int i = 0; i < arrayCafeteria.length(); i++) {
                object = arrayCafeteria.getJSONObject(i);

                try {
                    nombres.put(object.getString("id"), capitalisation(object.getString("nombre")));
                } catch (JSONException e) {
                    Log.e(TAG, "at listCafeterias problem with the name.");
                    nombres.put(object.getString("id"), "Cafetería no cargada");
                }
            }

            for (int i=0; i<arrayPuntuacion.length(); i++) {
                object = arrayPuntuacion.getJSONObject(i);
                id = object.getString("id");
                try {
                    votos = object.getInt("vGlobal");
                    global = Float.parseFloat(object.getString("global"))/votos;
                } catch (JSONException e) {
                    Log.e(TAG, "at listCafeterias problem with the global rating.");
                    global = 0f;
                    votos = 0;
                }

                row = new RowCafeteria(i,nombres.get(id),global,votos);
                result.add(row);

            }

        } catch (JSONException e) {
            Log.e(TAG, "at listCafeterias");
            e.printStackTrace();
        }

        return result;
    }

    public CafeteriaVO getCafeteria(int indice) {
        CafeteriaVO cafeteria = new CafeteriaVO();
        JSONObject object;

        try {
            JSONArray array = new JSONArray(loadData(cafeteriasFile));
            if (indice >= 0 && indice < array.length()) {
                object = array.getJSONObject(indice);

                cafeteria.setIndice(indice);

                try {
                    cafeteria.setNombre(object.getString("nombre"));
                } catch (JSONException e) {
                    Log.e(TAG, "at getCafeteria problem with the name.");
                    cafeteria.setNombre("Cafetería no cargada");
                }
            }

            array = new JSONArray((loadData(puntuacionesFile)));
            if(indice >= 0 && indice < array.length()) {
                object = array.getJSONObject(indice);

                try {
                    cafeteria.setVotosGlobal(object.getInt("vGlobal"));
                } catch (JSONException e) {
                    Log.e(TAG, "at getCafeteria problem with the global votes");
                    cafeteria.setVotosGlobal(-1);
                }

                try {
                    cafeteria.setVotosDiario(object.getInt("vDiario"));
                } catch (JSONException e) {
                    Log.e(TAG, "at getCafeteria problem with the daily votes");
                    cafeteria.setVotosDiario(-1);
                }

                try {
                    cafeteria.setPuntuacionGlobal(Float.parseFloat(object.getString("global")));
                } catch (JSONException e) {
                    Log.e(TAG, "at getCafeteria problem with the global rating.");
                    cafeteria.setPuntuacionGlobal(-1f);
                }

                try {
                    cafeteria.setPuntuacionDiario(object.getLong("diario"));
                } catch (JSONException e) {
                    Log.e(TAG, "at getCafeteria problem with the daily rating.");
                    cafeteria.setPuntuacionDiario(-1f);
                }

            }
        } catch (JSONException e) {
            Log.e(TAG, "at getCafeteria");
            e.printStackTrace();
        }

        return cafeteria;
    }

    public CafeteriaVO getInfo(CafeteriaVO cafeteria) {
        JSONObject object;

        try {
            JSONArray array = new JSONArray(loadData(cafeteriasFile));
            if (cafeteria.getIndice() >= 0 && cafeteria.getIndice() < array.length()) {
                object = array.getJSONObject(cafeteria.getIndice());

                // Horarios
                try {
                    JSONObject ob = object.getJSONObject("horarios");
                    cafeteria.setHorarios(getHorarios(ob));
                } catch (JSONException e) {
                    Log.e(TAG, "at getInfo problem with the schedule");
                }

                // Instalaciones
                try {
                    cafeteria.setBillar(object.getInt("billar"));
                } catch (JSONException e) {
                    Log.e(TAG, "at getInfo problem with the pool value");
                    cafeteria.setBillar(-1);
                }

                try {
                    cafeteria.setFutbolin(object.getInt("futbolin"));
                } catch (JSONException e) {
                    Log.e(TAG, "at getInfo problem with the table football value");
                    cafeteria.setFutbolin(-1);
                }

                try {
                    cafeteria.setMicroondas(object.getInt("microondas"));
                } catch (JSONException e) {
                    Log.e(TAG, "at getInfo problem with the microwave value");
                    cafeteria.setMicroondas(-1);
                }

                try {
                    cafeteria.setAforo(object.getInt("aforo"));
                } catch (JSONException e) {
                    Log.e(TAG, "at getInfo problem with the capacity value");
                    cafeteria.setAforo(-1);
                }

                // Contacto
                try {
                    String nombre = object.getString("responsable");
                    if (!nombre.equals("null"))
                        cafeteria.setResponsable(nombre);
                    else
                        cafeteria.setResponsable(" - ");

                } catch (JSONException e) {
                    Log.e(TAG, "at getInfo problem with the responsable name");
                    cafeteria.setResponsable(" - ");
                }

                try {
                    JSONObject ob = object.getJSONObject("telefonos");
                    cafeteria.setTelefonos(getTelefonos(ob));
                } catch (JSONException e) {
                    Log.e(TAG, "at getInfo problem with the telephones");
                    cafeteria.setTelefonos(null);
                }

                try {
                    cafeteria.setEmails(getEmails(object.getJSONObject("emails")));
                } catch (JSONException e) {
                    Log.e(TAG, "at getInfo problem with the emails");
                    cafeteria.setEmails(null);
                }
            }

        } catch (JSONException e) {
            Log.e(TAG, "at getInfo");
            e.printStackTrace();
        }

        return cafeteria;
    }

    private HashMap getEmails(JSONObject object) {
        HashMap emails = new HashMap();
        String key, value;
        Iterator<String> it = object.keys();

        while (it.hasNext()) {
            key = it.next();
            try {
                value = object.getString(key);
            } catch (JSONException e) {
                Log.e(TAG, "at getEmails there is a problema with the email");
                value = " - ";
                e.printStackTrace();
            }
            emails.put(capitalisation(key), value);
        }

        return emails;
    }

    private HashMap<String, Integer> getTelefonos(JSONObject object) {
        HashMap telefonos = new HashMap();
        String key;
        int value;
        Iterator<String> it = object.keys();

        while (it.hasNext()) {
            key = it.next();
            try {
                value = object.getInt(key);
            } catch (JSONException e) {
                Log.e(TAG, "at getEmails there is a problema with the email");
                value = 0;
                e.printStackTrace();
            }
            telefonos.put(capitalisation(key), value);
        }

        return telefonos;
    }

    private ArrayList<HorarioBO> getHorarios(JSONObject object) {
        ArrayList<HorarioBO> arrayHorarios = new ArrayList<>();
        Iterator<String>  itObject = object.keys();
        // TODO GORDO: Refactorizar Horarios, crear una clase Horarios que tenga la estructura y hacer un mapper?

        try {
            while (itObject.hasNext()) {
                String key = itObject.next();
                JSONArray array = object.getJSONArray(key);
                HorarioBO bo = new HorarioBO(key);

                for(int i=0; i<array.length(); i++) {
                    JSONObject conjunto = (JSONObject) array.get(i);

                    JSONArray periodo = conjunto.getJSONArray("periodo");
                    bo.addSeccion(String.valueOf(periodo.get(0)),String.valueOf(periodo.get(1)));

                    JSONObject dias = conjunto.getJSONObject("dias");
                    Iterator<String> itConjunto = dias.keys();
                    while(itConjunto.hasNext()) {
                        String diaNombre = itConjunto.next();
                        if(diaNombre.equals("L-V")) {
                            try {
                                bo.setSeccionLunesAViernes(i,dias.getString(diaNombre));
                            } catch (Exception e) {
                                Log.e(TAG,"at getHorarios with the day schelude");
                                e.printStackTrace();
                            }
                        } else {
                            String dia;
                            switch (diaNombre) {
                                case "L":
                                    dia = HorarioBO.SEM_LUN;
                                    break;
                                case "M":
                                    dia = HorarioBO.SEM_MAR;
                                    break;
                                case "X":
                                    dia = HorarioBO.SEM_MIER;
                                    break;
                                case "J":
                                    dia = HorarioBO.SEM_JUE;
                                    break;
                                case "V":
                                    dia = HorarioBO.SEM_VIER;
                                    break;
                                case "S":
                                    dia = HorarioBO.SEM_SAB;
                                    break;
                                case "D":
                                    dia = HorarioBO.SEM_DOM;
                                    break;
                                default:
                                    dia = "";
                                    break;
                            }

                            if(!dia.equals("")) {
                                try {
                                    bo.setSeccionHorario(i,dia,dias.getString(diaNombre));
                                } catch (Exception e) {
                                    Log.e(TAG,"at getHorarios with the day schelude");
                                    e.printStackTrace();
                                }
                            }

                        }
                    }
                }
                arrayHorarios.add(bo);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return arrayHorarios;
    }

    public int getID(String cafeteria) {
        ArrayList<String> cafeterias = new ArrayList<>();
        JSONObject object;
        int id = 0;

        try {
            JSONArray array = new JSONArray(loadData(cafeteriasFile));

            for(int i=0; i<array.length(); i++) {
                object = array.getJSONObject(i);
                try {
                    String nombre = capitalisation(object.getString("nombre"));
                    if(cafeteria.equals(nombre)) {
                        id = i;
                        break;
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "at listCafeterias problem with the name.");
                    cafeterias.add("Cafetería no cargada");
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "at getListado problem open the file "+cafeteriasFile);
            e.printStackTrace();
        }

        return id;
    }

    private class NameComparator implements Comparator<RowCafeteria> {
       @Override
        public int compare(RowCafeteria lhs, RowCafeteria rhs) {
            return lhs.getNombre().compareTo(rhs.getNombre());
        }
    }

    private class ValuationComparator implements Comparator<RowCafeteria> {
        @Override
        public int compare(RowCafeteria lhs, RowCafeteria rhs) {
            return Float.compare(lhs.getGlobal(),rhs.getGlobal());
        }
    }
}
