package cphcarolina.comerua.dao;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cphcarolina.comerua.vo.ProductoVO;

/**
 * Created by Carolina on 04/03/2015.
 */
public class ProductoDAO extends DataManager {
    private final String TAG = "ProductoDAO";
    private final String productosFile = "productos.json";

    public ProductoDAO(Context context) {
        super(context);
    }

    public ArrayList<ProductoVO> listProductos() {
        ArrayList<ProductoVO> result = new ArrayList<>();
        ProductoVO producto;
        JSONObject object;
        String nombre, categoria;
        float precio;

        try {
            JSONArray array = new JSONArray(loadData(productosFile));
            for (int i = 0; i < array.length(); i++) {
                object = array.getJSONObject(i);

                try {
                    nombre = capitalisation(object.getString("nombre"));
                } catch (JSONException e) {
                    Log.e(TAG, "at listProductos problem with the name.");
                    nombre = "Cafetería no cargada";
                }

                try {
                    precio = setPrecio(object.getString("precio (Eur)"));

                } catch (JSONException e) {
                    Log.e(TAG, "at listProductos problem with the price.");
                    precio = 0f;
                }

                try {
                    categoria = capitalisation(object.getString("categoría"));
                } catch (JSONException e) {
                    Log.e(TAG, "at listProductos problem with the category.");
                    categoria = "";
                }

                producto = new ProductoVO(nombre, precio);
                producto.setCategoria(categoria);
                result.add(producto);
            }

        } catch (JSONException e) {
            Log.e(TAG, "at listProductos");
            e.printStackTrace();
        }

        return result;
    }

    public ProductoVO getByName(String name) {
        // TODO que el get sea por Id y no por nombre
        ProductoVO producto = null;
        JSONObject object;
        String nombre, descripcion="";
        float precio = 0f;
        boolean encontrado = false;

        try {
            JSONArray array = new JSONArray(loadData(productosFile));
            for (int i = 0; i < array.length() && !encontrado; i++) {
                object = array.getJSONObject(i);

                try {
                    nombre = capitalisation(object.getString("nombre"));

                    if(name.equals(nombre)) {
                        try {
                            precio = setPrecio(object.getString("precio (Eur)"));

                        } catch (JSONException e) {
                            Log.e(TAG, "at getByName problem with the price.");
                            precio = 0f;
                        }

                        try {
                            String desc = object.getString("descripción");
                            if( desc == null) {
                                descripcion = "No existe descripción";
                            } else if(desc.equals("")) {
                                descripcion = "No existe descripción";
                            } else {
                                descripcion = capitalisation(desc);
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "at getByName problem with the desccription.");
                            descripcion = "Descripción no cargada";
                        }

                        // TODO get global y diario
                        producto = new ProductoVO(nombre, precio);
                        producto.setDescripcion(descripcion);
                        encontrado = true;
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "at getByName problem with the name.");
                }


            }

        } catch (JSONException e) {
            Log.e(TAG, "at getByName");
            e.printStackTrace();
        }


        return producto;
    }

    private float setPrecio(String strPrecio) {
        float fPrecio;
        String[] partes = strPrecio.split(",");
        String strPrecioFinal = partes[0] + "." + partes[1];
        fPrecio = Float.parseFloat(strPrecioFinal);

        return fPrecio;
    }
}