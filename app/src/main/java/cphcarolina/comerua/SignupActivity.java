package cphcarolina.comerua;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cphcarolina.comerua.dao.UsuarioDAO;

public class SignupActivity extends Activity {
    EditText editNombre, editEmail, editContrasenia, editContraseniaRepe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setTitle(R.string.app_name);
        TextView titulo = (TextView) findViewById(R.id.textCafeteriaTitle);
        titulo.setText("Registro");
        titulo.setTextColor(getResources().getColor(R.color.cabecera));
    }


    @Override
    protected void onStart() {
        super.onStart();

        editNombre = (EditText) findViewById(R.id.editNombre);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editContrasenia = (EditText) findViewById(R.id.editContrasenia);
        editContraseniaRepe = (EditText) findViewById(R.id.editContraseniaRepe);

        Button btnRegistro = (Button) findViewById(R.id.btnSignup);
        TextView lblLogin = (TextView) findViewById(R.id.lblLogin);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UsuarioDAO user = new UsuarioDAO(SignupActivity.this);
                String nombre = String.valueOf(editNombre.getText());
                String email = String.valueOf(editEmail.getText());
                String contrasenia = String.valueOf(editContrasenia.getText());
                String contraseniaRepe = String.valueOf(editContraseniaRepe.getText());

                if (nombre == null || email == null || contrasenia == null ||
                        contraseniaRepe == null) {
                    Toast.makeText(SignupActivity.this, "Rellene todos los campos para realizar el registro",
                            Toast.LENGTH_LONG).show();
                } else if (!contrasenia.equals(contraseniaRepe)) {
                    Toast.makeText(SignupActivity.this, "Las contraseñas no coinciden",
                            Toast.LENGTH_LONG).show();
                } else if (user.signup(nombre, email, contrasenia)) {
                    SharedPreferences pref = getSharedPreferences("PreferenciasComerUA", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("nombre", nombre);
                    editor.putBoolean("isLogged", true);
                    editor.commit();

                    Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });

        lblLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
