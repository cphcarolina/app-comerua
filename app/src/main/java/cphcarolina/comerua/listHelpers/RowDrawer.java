package cphcarolina.comerua.listHelpers;

/**
 * Created by Carolina on 20/04/2015.
 */
public class RowDrawer {
    private String itemName;
    private int itemImage;
    private boolean isBottom;
    private boolean isUser;
    private boolean isHome;

    public RowDrawer(String name) {
        itemName = name;
        itemImage = -1;
        isBottom = false;
        isUser = false;
        isHome = false;
    }

    public RowDrawer(String name, int img) {
        itemName = name;
        itemImage = img;
        isBottom = false;
        isUser = false;
        isHome = false;
    }

    public RowDrawer(String name, boolean bottom) {
        itemName = name;
        itemImage = -1;
        isBottom = bottom;
        isUser = false;
        isHome = false;
    }

    public int getItemImage() {
        return itemImage;
    }

    public void setItemImage(int itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isBottom() {
        return isBottom;
    }

    public void setBottom(boolean isBottom) {
        this.isBottom = isBottom;
    }

    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean isUser) {
        this.isUser = isUser;
    }

    public boolean isHome() {
        return isHome;
    }

    public void setHome(boolean isHome) {
        this.isHome = isHome;
    }
}
