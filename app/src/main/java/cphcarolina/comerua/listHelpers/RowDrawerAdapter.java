package cphcarolina.comerua.listHelpers;

import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.sql.SQLClientInfoException;
import java.util.List;

import cphcarolina.comerua.R;

/**
 * Created by Carolina on 20/04/2015.
 */
public class RowDrawerAdapter extends ArrayAdapter<RowDrawer> {
    private static final String TAG = "RowDrawerAdapter";
    private static int convertViewCounter = 0;
    private List<RowDrawer> data;
    private LayoutInflater inflater = null;

    static class ViewHolder {
        TextView userName, homeText, itemName, itemBottom;
        ImageView userImg, homeImg;
        LinearLayout userLayout, homeLayout;
    }


    public RowDrawerAdapter(Context context, int layoutResourceID,
                               List<RowDrawer> listItems) {
        super(context, layoutResourceID, listItems);
        Log.v(TAG, "Constructing RowCafeteriaAdapter");
        this.data = listItems;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        Log.v(TAG, "in getCount()");
        return data.size();
    }

    @Override
    public RowDrawer getItem(int position) {
        Log.v(TAG, "in getItem() for position " + position);
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.v(TAG, "in getItemId() form position " + position);
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        Log.v(TAG, "in getView() for position " + position);

        if (convertView == null) {
            Log.v(TAG, convertViewCounter + " convertViews have been created");
            convertView = inflater.inflate(R.layout.row_drawer, null);
            convertViewCounter++;
            holder = new ViewHolder();

            holder.userName = (TextView) convertView.findViewById(R.id.userName);
            holder.userImg = (ImageView) convertView.findViewById(R.id.userImg);
            holder.homeText = (TextView) convertView.findViewById(R.id.homeText);
            holder.homeImg = (ImageView) convertView.findViewById(R.id.homeImg);
            holder.itemName = (TextView) convertView.findViewById(R.id.textItem);
            holder.itemBottom = (TextView) convertView.findViewById(R.id.textBottom);

            holder.userLayout = (LinearLayout) convertView.findViewById(R.id.userLayout);
            holder.homeLayout = (LinearLayout) convertView.findViewById(R.id.homeLayout);

            convertView.setTag(holder);
        } else {
            Log.v(TAG, "convertView being recycled");
            holder = (ViewHolder) convertView.getTag();
        }

        RowDrawer dItem = (RowDrawer) this.data.get(position);

        if(dItem.isBottom()) {
            holder.userLayout.setVisibility(LinearLayout.INVISIBLE);
            holder.homeLayout.setVisibility(LinearLayout.INVISIBLE);
            holder.itemName.setVisibility(TextView.INVISIBLE);
            holder.itemBottom.setVisibility(TextView.VISIBLE);

            holder.itemBottom.setText(dItem.getItemName());
        } else if (dItem.isHome()) {
            holder.userLayout.setVisibility(LinearLayout.VISIBLE);
            holder.homeLayout.setVisibility(LinearLayout.INVISIBLE);
            holder.itemName.setVisibility(TextView.INVISIBLE);
            holder.itemBottom.setVisibility(TextView.INVISIBLE);

            holder.userName.setText(dItem.getItemName());
            holder.userImg.setImageDrawable(convertView.getResources().getDrawable(dItem.getItemImage()));
        } else if(dItem.isHome()) {
            holder.userLayout.setVisibility(LinearLayout.INVISIBLE);
            holder.homeLayout.setVisibility(LinearLayout.VISIBLE);
            holder.itemName.setVisibility(TextView.INVISIBLE);
            holder.itemBottom.setVisibility(TextView.INVISIBLE);

            holder.homeText.setText(dItem.getItemName());
            holder.homeImg.setImageDrawable(convertView.getResources().getDrawable(dItem.getItemImage()));
        } else {
            holder.userLayout.setVisibility(LinearLayout.INVISIBLE);
            holder.homeLayout.setVisibility(LinearLayout.INVISIBLE);
            holder.itemName.setVisibility(TextView.VISIBLE);
            holder.itemBottom.setVisibility(TextView.INVISIBLE);

            holder.itemName.setText(dItem.getItemName());
        }

        return convertView;
    }

}
