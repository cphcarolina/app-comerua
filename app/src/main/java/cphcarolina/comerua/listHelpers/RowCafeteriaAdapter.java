package cphcarolina.comerua.listHelpers;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import cphcarolina.comerua.R;

/**
 * Created by Carolina on 18/02/2015.
 */
public class RowCafeteriaAdapter extends BaseAdapter {
    private static final String TAG = "RowCafeteriaAdapter";
    private static int convertViewCounter = 0;
    private ArrayList<RowCafeteria> data;
    private LayoutInflater inflater = null;

    public RowCafeteriaAdapter(Context context, ArrayList<RowCafeteria> values) {
        Log.v(TAG, "Constructing RowCafeteriaAdapter");
        this.data = values;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        Log.v(TAG, "in getCount()");
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        Log.v(TAG, "in getItem() for position " + position);
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.v(TAG, "in getItemId() form position " + position);
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        Log.v(TAG, "in getView() for position " + position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_cafeteria, null);
            convertViewCounter++;
            Log.v(TAG, convertViewCounter + " convertViews have been created");

            holder = new ViewHolder();
            holder.tvLabel = (TextView) convertView.findViewById(R.id.label);
            holder.rbGlobal = (RatingBar) convertView.findViewById(R.id.ratingGlobal);
            holder.tvVotos = (TextView) convertView.findViewById(R.id.votos);
            convertView.setTag(holder);
        } else {
            Log.v(TAG, "convertView being recycled");
            holder = (ViewHolder) convertView.getTag();
        }

        Log.v(TAG, data.get(position).getNombre());
        holder.tvLabel.setText(data.get(position).getNombre());
        holder.rbGlobal.setRating(data.get(position).getGlobal());
        holder.tvVotos.setText(String.valueOf(data.get(position).getVotos()));
        holder.rbGlobal.setIsIndicator(true);

        holder.rbGlobal.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Log.d(TAG, "Ratingbar modificado a "+rating);
                holder.rbGlobal.setRating(rating);
            }
        });


        switch (data.get(position).getIndex()) {
            case 0:
                convertView.setBackgroundResource(R.color.cafeteria1);
                break;
            case 1:
                convertView.setBackgroundResource(R.color.cafeteria2);
                break;
            case 2:
                convertView.setBackgroundResource(R.color.cafeteria3);
                break;
            case 3:
                convertView.setBackgroundResource(R.color.cafeteria4);
                break;
            case 4:
                convertView.setBackgroundResource(R.color.cafeteria5);
                break;
            case 5:
                convertView.setBackgroundResource(R.color.cafeteria6);
                break;
            case 6:
                convertView.setBackgroundResource(R.color.cafeteria7);
                break;
            default:
                convertView.setBackgroundColor(Color.WHITE);
                break;
        }

        return convertView;
    }

    static class ViewHolder {
        TextView tvLabel;
        RatingBar rbGlobal;
        TextView tvVotos;
    }
}
