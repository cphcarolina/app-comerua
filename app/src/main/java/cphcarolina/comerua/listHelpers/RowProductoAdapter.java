package cphcarolina.comerua.listHelpers;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import cphcarolina.comerua.R;

/**
 * Created by Carolina on 26/02/2015.
 */
public class RowProductoAdapter extends BaseAdapter {
    private static final String TAG = "RowProductoAdapter";
    private static int convertViewCounter = 0;
    private ArrayList<RowProducto> data;
    private LayoutInflater inflater = null;

    public RowProductoAdapter(Context context, ArrayList<RowProducto> values) {
        Log.v(TAG, "Constructing RowCafeteriaAdapter");
        for (RowProducto row : values) {
            Log.v(TAG, " -> " + row.getNombre());
        }

        this.data = values;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        Log.v(TAG, "in getCount() " + data.size());
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        Log.v(TAG, "in getItem() for position " + position);
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.v(TAG, "in getItemId() form position " + position);
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Log.v(TAG, "in getView() for position " + position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_producto, null);
            convertViewCounter++;
            Log.v(TAG, convertViewCounter + " convertViews have been created");

            holder = new ViewHolder();
            holder.tvNombre = (TextView) convertView.findViewById(R.id.textNombre);
            holder.tvPrecio = (TextView) convertView.findViewById(R.id.textPrecio);
            holder.rbGlobal = (RatingBar) convertView.findViewById(R.id.ratingGlobal);
            convertView.setTag(holder);
        } else {
            Log.v(TAG, "convertView being recycled");
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvNombre.setText(data.get(position).getNombre());
        holder.tvPrecio.setText(data.get(position).getStrPrecio());
        holder.rbGlobal.setRating(data.get(position).getGlobal());

        if (position % 2 == 1) {
            convertView.setBackgroundColor(Color.rgb(241, 245, 248));
        } else {
            convertView.setBackgroundColor(Color.WHITE);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView tvNombre;
        TextView tvPrecio;
        RatingBar rbGlobal;
    }
}
