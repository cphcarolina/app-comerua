package cphcarolina.comerua.listHelpers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Carolina on 26/02/2015.
 */
public class RowProducto implements Parcelable {
    private String TAG = "RowProducto";
    private String nombre;
    private float precio;
    private float global;

    public RowProducto(String nombre, float precio, float global) {
        this.nombre = nombre;
        this.precio = precio;
        this.global = global;
    }

    public RowProducto(Parcel in) {
        this.nombre = in.readString();
        this.precio = Float.parseFloat(in.readString());
        this.global = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getNombre());
        dest.writeString(getStrPrecio());
        dest.writeFloat(getGlobal());
    }

    public String getNombre() {
        return nombre;
    }

    public float getGlobal() {
        return global;
    }

    public String getStrPrecio() {
        return String.valueOf(precio) + "€";
    }

    public static final Parcelable.Creator<RowProducto> CREATOR = new Parcelable.Creator<RowProducto>() {
        public RowProducto createFromParcel(Parcel in) {
            return new RowProducto(in);
        }

        public RowProducto[] newArray(int size) {
            return new RowProducto[size];
        }
    };
    // TODO GETTERS (no recuerdo a qué me refería aquí con los GETTERS si están arriba...


}
