package cphcarolina.comerua.listHelpers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Carolina on 18/02/2015.
 */
public class RowCafeteria implements Parcelable {
    public static final Creator<RowCafeteria> CREATOR = new Creator<RowCafeteria>() {
        public RowCafeteria createFromParcel(Parcel in) {
            return new RowCafeteria(in);
        }

        public RowCafeteria[] newArray(int size) {
            return new RowCafeteria[size];
        }
    };

    private static final String TAG = "RowCafeteria";
    private String nombre;
    private int votos;
    private float global;
    private int index;

    public RowCafeteria(int index, String nombre, float global, int votos) {
        this.nombre = nombre;
        this.votos = votos;
        this.global = global;
        this.index = index;
    }

    public RowCafeteria(Parcel in) {
        this.nombre = in.readString();
        this.global = in.readFloat();
        this.votos = in.readInt();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getGlobal() {
        return global;
    }

    public void setGlobal(float global) {
        this.global = global;
    }

    public int getVotos() {
        return votos;
    }

    public void setVotos(int votos) {
        this.votos = votos;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getNombre());
        dest.writeFloat(getGlobal());
        dest.writeInt(getVotos());
        dest.writeInt(getIndex());
    }
}
