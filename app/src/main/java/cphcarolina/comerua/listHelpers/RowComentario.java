package cphcarolina.comerua.listHelpers;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Carolina on 25/02/2015.
 */
public class RowComentario implements Parcelable {
    public static final Parcelable.Creator<RowComentario> CREATOR = new Parcelable.Creator<RowComentario>() {
        public RowComentario createFromParcel(Parcel in) {
            return new RowComentario(in);
        }

        public RowComentario[] newArray(int size) {
            return new RowComentario[size];
        }
    };
    private String TAG = "RowComentario";
    private SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy H:m");
    private String usuario;
    private Date fecha;
    private String comentario;

    public RowComentario(String usuario, String fecha, String texto) {
        try {
            this.usuario = usuario;
            this.fecha = formatoDeFecha.parse(fecha);
            this.comentario = texto;
        } catch (ParseException e) {
            this.fecha = Calendar.getInstance().getTime();
            e.printStackTrace();
        }
    }

    public RowComentario(Parcel in) {
        try {
            this.usuario = in.readString();
            this.fecha = formatoDeFecha.parse(in.readString());
            this.comentario = in.readString();
        } catch (ParseException e) {
            this.fecha = Calendar.getInstance().getTime();
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getUsuario());
        dest.writeString(getStrFecha());
        dest.writeString(getComentario());
    }

    public String getUsuario() {
        return usuario;
    }

    public String getStrFecha() {
        return formatoDeFecha.format(fecha);
    }

    public String getComentario() {
        return comentario;
    }

    public String toString() {
        Log.v(TAG, "at toString " + usuario);
        return "[" + usuario + ", " + fecha + ", " + comentario + "]";
    }
}