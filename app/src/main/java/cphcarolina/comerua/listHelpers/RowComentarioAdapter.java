package cphcarolina.comerua.listHelpers;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import cphcarolina.comerua.R;

/**
 * Created by Carolina on 26/02/2015.
 */

// TODO leer https://thinkandroid.wordpress.com/2010/01/11/custom-cursoradapters/
public class RowComentarioAdapter extends BaseAdapter {
    private static final String TAG = "RowComentarioAdapter";
    private static int convertViewCounter = 0;
    private ArrayList<RowComentario> data;
    private LayoutInflater inflater = null;

    public RowComentarioAdapter(Context context, ArrayList<RowComentario> values) {
        Log.v(TAG, "Constructing RowComentarioAdapter");
        this.data = values;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        Log.v(TAG, "in getCount()");
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        Log.v(TAG, "in getItem() for position " + position);
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.v(TAG, "in getItemId() form position " + position);
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Log.v(TAG, "in getView() for position " + position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_comentario, null);
            convertViewCounter++;
            Log.v(TAG, convertViewCounter + " convertViews have been created");

            holder = new ViewHolder();
            holder.tvUser = (TextView) convertView.findViewById(R.id.textUser);
            holder.tvDate = (TextView) convertView.findViewById(R.id.textDate);
            holder.tvComment = (TextView) convertView.findViewById(R.id.textComment);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            Log.v(TAG, "converView being recycled");
        }

        holder.tvUser.setText(data.get(position).getUsuario());
        holder.tvDate.setText(data.get(position).getStrFecha());
        holder.tvComment.setText(data.get(position).getComentario());

        if (position % 2 == 1) {
            // azul pálido R: 241, G: 245, B: 248
            // azul claro R: 216, G: 229, B: 234
            convertView.setBackgroundColor(Color.rgb(241, 245, 248));
        } else {
            convertView.setBackgroundColor(Color.WHITE);
        }


        return convertView;
    }

    public boolean isEnabled(int position) {
        super.isEnabled(position);
        return false;
    }

    static class ViewHolder {
        TextView tvUser;
        TextView tvDate;
        TextView tvComment;
    }

}


