package cphcarolina.comerua;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import cphcarolina.comerua.dao.CafeteriaDAO;
import cphcarolina.comerua.fragments.BaseFragment;
import cphcarolina.comerua.fragments.CafeteriaPrincipal;
import cphcarolina.comerua.fragments.ListadoCafeterias;
import cphcarolina.comerua.listHelpers.RowDrawer;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "MainActivity";

    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private ListView leftList;
    private ActionBar actionBar;
    private FragmentManager fragmentMng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //checkIfIsLogged();

        /** ActionBar **/
        actionBar = getSupportActionBar();
        actionBar.setDefaultDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        /** Fragment principal **/
        fragmentMng = getFragmentManager();
        Fragment listadoCafeterias = new ListadoCafeterias();
        fragmentMng.beginTransaction()
                .replace(R.id.content_frame, listadoCafeterias)
                .addToBackStack(ListadoCafeterias.FRAGMENT_NAME)
                .commit();

        /** Lista Lateral **/
        /*ArrayList data = new ArrayList<RowDrawer>();
        leftList = (ListView) findViewById(R.id.left_list);

        RowDrawer home = new RowDrawer("ComerUA", R.drawable.ic_launcher);
        home.setHome(true);

        RowDrawer user = new RowDrawer("Usuario nuevo", R.drawable.ic_launcher);
        user.setUser(true);

        data.add(user);
        data.add(home);
        String[] cafeterias = new CafeteriaDAO(this).getListado();
        for(int i=0; i< cafeterias.length; i++) {
            data.add(new RowDrawer(cafeterias[i]));
        }
        data.add(new RowDrawer("¿Quiénes somos?", true));*/

        String[] cafeterias = new CafeteriaDAO(this).getListado();
        leftList = (ListView) findViewById(R.id.left_list);
        leftList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.row_leftlist,cafeterias));
        leftList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String nombre = (String) parent.getItemAtPosition(position);
                selectItem(nombre, position);
            }
        });

        /** Cajón Lateral **/
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer,
                R.string.drawer_open, R.string.drawer_closed) {

            public void onDrawerClosed(View view) {
                actionBar.setTitle(R.string.app_name);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view) {
                actionBar.setTitle(R.string.app_name);
                invalidateOptionsMenu();
            }
        };
        drawer.setDrawerListener(toggle);
    }

    private void checkIfIsLogged() {
        SharedPreferences pref = getSharedPreferences("PreferenciasComerUA", Context.MODE_PRIVATE);
        boolean isLogged = pref.getBoolean("isLogged", false);

        if (!isLogged) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    private void selectItem(String cafeteria, int position) {
        Bundle args = new Bundle();
        int id = new CafeteriaDAO(this).getID(cafeteria);
        args.putInt(BaseFragment.ARG_CAFETERIA_INDEX,id);

        Fragment fragment = new CafeteriaPrincipal();
        fragment.setArguments(args);
        fragmentMng.beginTransaction()
                .replace(R.id.content_frame,fragment)
                .addToBackStack(CafeteriaPrincipal.FRAGMENT_NAME)
                .commit();

        leftList.setItemChecked(position,true);
        drawer.closeDrawer(leftList);
    }

    @Override
    public void onBackPressed() {
        int n = fragmentMng.getBackStackEntryCount();
        if(n==1) {
            Log.d("BackStack", "Se carga el primero");
            super.onBackPressed();
        } else {
            fragmentMng.popBackStack();
            Log.d("BackStack","Se carga el anterior");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = drawer.isDrawerOpen(leftList);
        MenuItem item = menu.findItem(R.id.action_search);
        if(item!=null) item.setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item)) return true;

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                Log.d(TAG, "Home Item Selected");
                Toast.makeText(this,"Home Item Selected",Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_search:
                Log.d(TAG, "Search Item Selected");
                Toast.makeText(this,"El filtrado de productos está en contrucción",Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
