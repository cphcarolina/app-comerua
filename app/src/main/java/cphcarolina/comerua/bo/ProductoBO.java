package cphcarolina.comerua.bo;

import android.content.Context;

import java.util.ArrayList;

import cphcarolina.comerua.dao.ProductoDAO;
import cphcarolina.comerua.listHelpers.RowProducto;
import cphcarolina.comerua.vo.ProductoVO;

/**
 * Product Business Object
 * Created by Carolina on 04/03/2015.
 */
public class ProductoBO {
    private ProductoDAO productoDAO;
    private ArrayList<String> categorias;


    public ProductoBO(Context context) {
        this.productoDAO = new ProductoDAO(context);
    }

    public ArrayList<RowProducto> getByCategoria(String categoria) {
        // TODO el listado también ha de depender de la cafetería, aunque ahora no lo haga
        ArrayList<RowProducto> result = new ArrayList<>();
        RowProducto row;
        ArrayList<ProductoVO> productos = productoDAO.listProductos();

        for (ProductoVO producto : productos) {
            if (producto.getCategoria().contentEquals(categoria)) {
                row = new RowProducto(producto.getNombre(), producto.getPrecio(), producto.getPuntuacionGlobal());
                result.add(row);
            }
        }
        return result;
    }
}
