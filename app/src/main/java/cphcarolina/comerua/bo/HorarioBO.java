package cphcarolina.comerua.bo;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Schedule Business Object
 * Created by Carolina on 28/02/2015.
 */
public class HorarioBO {
    private final String TAG = "HorarioBO";
    private final String[] diasSemana;
    public static final String HORARIO_CLOSED = "Cerrado";
    private static final String SPLIT_HORARIO = "-";
    private static final String SPLIT_HORA = ".";
    public static final String SEM_LUN = "Lunes";
    public static final String SEM_MAR = "Martes";
    public static final String SEM_MIER = "Miercoles";
    public static final String SEM_JUE = "Jueves";
    public static final String SEM_VIER = "Viernes";
    public static final String SEM_SAB = "Sabado";
    public static final String SEM_DOM = "Domingo";

    private String nombre;
    private List<Seccion> secciones;


    public HorarioBO(String nombre) {
        this.nombre = nombre;
        secciones = new ArrayList<>();
        diasSemana = new String[7];
        diasSemana[0] = SEM_LUN;
        diasSemana[1] = SEM_MAR;
        diasSemana[2] = SEM_MIER;
        diasSemana[3] = SEM_JUE;
        diasSemana[4] = SEM_VIER;
        diasSemana[5] = SEM_SAB;
        diasSemana[6] = SEM_DOM;
    }

    public int addSeccion(String strInicio, String strFin) {
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        Date inicio, fin;
        int posicion = -1;

        try {
            inicio = formatDate.parse(strInicio);
            fin = formatDate.parse(strFin);

            Seccion nueva = new Seccion(inicio, fin);
            secciones.add(nueva);
            posicion = secciones.indexOf(nueva);

        } catch (ParseException e) {
            Log.e(TAG, "at addSeccion wrong format dates");
            e.printStackTrace();
        }

        return posicion;
    }

    private boolean isDia(String dia) throws Exception {
        boolean ok = false;

        for (String aDiasSemana : diasSemana) {
            if (dia.compareTo(aDiasSemana) == 0) {
                ok = true;
            }
        }

        if (!ok) {
            throw new Exception(TAG + " isDia: El día no es correcto.");
        }

        return ok;
    }

    /**
     * Modifies the schedule of one concrete day of the week
     *
     * @param seccion index identifier section of time
     * @param dia     day of the week, valid values: 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo')
     * @param hora    new schedule
     * @throws Exception if the index section does not exists or if the day is not correct (see valid values)
     */
    public void setSeccionHorario(int seccion, String dia, String hora) throws Exception {
        if (seccion >= 0 && seccion < secciones.size()) {
            if (isDia(dia)) {
                secciones.get(seccion).setDia(dia, hora);
            }
        } else {
            throw new Exception("HorarioBO.setHorarioSeccion: índice incorrecto de sección");
        }
    }

    /**
     * Modifies the schedule of monday to friday
     *
     * @param seccion index identifier section of time
     * @param hora    new schedule
     * @throws Exception if the index section does not exists
     */
    public void setSeccionLunesAViernes(int seccion, String hora) throws Exception {
        if (seccion >= 0 && seccion < secciones.size()) {
            secciones.get(seccion).setLunesViernes(hora);
        } else {
            throw new Exception(TAG + " setHorarioSeccion: índice incorrecto de sección");
        }
    }

    public String getNombre() {
        return nombre;
    }

    /**
     * Obtain the correct schedule for the actual period of time
     *
     * @return null if there is no schedule
     */
    public HashMap getHorario() {
        for (Seccion sec : secciones) {
            if (sec.isActual()) {
                return sec.getHorario();
            }
        }
        return null;
    }

    public boolean isAbierto() throws Exception {
        int dia = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        String horario = "";

        for (Seccion sec : secciones) {
            if (sec.isActual()) {
                if (dia == 0) horario = sec.getDia(diasSemana[6]); // domingo
                else {
                    for (int i = 0; i < diasSemana.length; i++) {
                        if ((dia - 1) == i) horario = sec.getDia(diasSemana[i]);
                    }
                }
                return onTime(horario);
            }
        }
        return false;
    }

    private boolean onTime(String horario) throws Exception {
        String[] strHoras, strInicio, strFin;
        Calendar actual, inicio, fin;

        strHoras = horario.split(SPLIT_HORARIO);
        if (strHoras.length == 2) {

            strInicio = strHoras[0].split(SPLIT_HORA);
            strFin = strHoras[1].split(SPLIT_HORA);
            if (strInicio.length == 2 && strFin.length == 2) {
                actual = Calendar.getInstance();
                inicio = actual;
                fin = actual;

                inicio.set(Calendar.HOUR_OF_DAY, Integer.parseInt(strInicio[0]));
                inicio.set(Calendar.MINUTE, Integer.parseInt(strInicio[1]));
                inicio.set(Calendar.SECOND, 0);

                fin.set(Calendar.HOUR_OF_DAY, Integer.parseInt(strFin[0]));
                fin.set(Calendar.MINUTE, Integer.parseInt(strFin[1]));
                fin.set(Calendar.SECOND, 0);

                if (inicio.before(actual) && actual.before(fin)) return true;

            } else throw new Exception(TAG + " onTime: formato incorrecto de la hora.");
        } else throw new Exception(TAG + " onTime: formato incorrecto de las horas.");

        return false;
    }

    private class Seccion {
        private Date fechaInicio;
        private Date fechaFin;

        private HashMap<String, String> horario;

        public Seccion(Date fechaInicio, Date fechaFin) {
            this.fechaInicio = fechaInicio;
            this.fechaFin = fechaFin;
            iniciarHorario();
        }

        private void iniciarHorario() {
            horario = new HashMap<>();

            for (String aDiasSemana : diasSemana) {
                horario.put(aDiasSemana, HORARIO_CLOSED);
            }
        }

        public void setLunesViernes(String horas) {
            for (int i = 0; i < 5; i++) {
                horario.put(diasSemana[i], setHoras(horas));
            }
        }

        public void setDia(String dia, String hora) {
            horario.put(dia, setHoras(hora));
        }

        public HashMap getHorario() {
            return (HashMap) sortByDays(horario);
        }

        private  Map<String,String> sortByDays(Map<String, String> map){
            List<String> keys = new LinkedList<>(map.keySet());
            Collections.sort(keys, new DaysComparator());

            Map<String,String> sortedMap = new LinkedHashMap<>();
            for(String key: keys){
                sortedMap.put(key, map.get(key));
            }

            return sortedMap;
        }

        private class DaysComparator implements Comparator<String> {
            @Override
            public int compare(String o1, String o2) {

                if(o1.equals(o2)) return 0;

                if(o1.equals(SEM_LUN) || o2.equals(SEM_DOM)) return -1;
                if(o1.equals(SEM_DOM) || o2.equals(SEM_LUN)) return 1;

                if(o1.equals(SEM_MAR) || o2.equals(SEM_SAB)) return -1;
                if(o1.equals(SEM_SAB) || o2.equals(SEM_MAR)) return 1;

                if(o1.equals(SEM_MIER) || o2.equals(SEM_VIER)) return -1;
                if(o1.equals(SEM_VIER) || o2.equals(SEM_MIER)) return 1;

                return 0;
            }
        }

        private String setHoras(String horas) {
            String result = "";
            String[] strHoras = new String[2];

            try {
                JSONArray array = new JSONArray(horas);
                strHoras[0] = (String) array.get(0);
                strHoras[1] = (String) array.get(1);

                result = strHoras[0] + " - "+ strHoras[1];
            } catch (JSONException e) {
                e.printStackTrace();
                result = HORARIO_CLOSED;
            }

            return result;
        }

        public boolean isActual() {
            Date actual = Calendar.getInstance().getTime();

            return actual.compareTo(fechaInicio) >= 0
                    && actual.compareTo(fechaFin) <= 0;
        }

        public String getDia(String dia) {
            return horario.get(dia);
        }
    }
}
