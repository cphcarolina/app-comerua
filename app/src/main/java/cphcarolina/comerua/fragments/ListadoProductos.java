package cphcarolina.comerua.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cphcarolina.comerua.R;
import cphcarolina.comerua.bo.ProductoBO;
import cphcarolina.comerua.listHelpers.RowProducto;
import cphcarolina.comerua.listHelpers.RowProductoAdapter;

public class ListadoProductos extends BaseFragment {
    private static final String TAG = "Activity ListadoProductos";
    public static final String ARG_CATEGORIA_NAME = "categoriaNombre";
    public static final String FRAGMENT_NAME = "listado";

    private ListView listView;
    private ProgressBar progressBar;
    private String categoria;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        view = inflater.inflate(R.layout.fragment_listado_productos,container,false);
        categoria = getArguments().getString(ARG_CATEGORIA_NAME,null);

        TextView title = (TextView) findViewById(R.id.textCafeteriaTitle);
        if(categoria==null) title.setText("Problema al cargar los productos");
        else title.setText(categoria);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(listener);
        new CargaProductosAsync().execute();

        return view;
    }


    private AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "listlistener pressed");
            RowProducto producto = (RowProducto) listView.getAdapter().getItem(position);

            Bundle args = new Bundle();
            args.putInt(ARG_CAFETERIA_INDEX,indiceCafeteria);
            args.putString(ProductoDetalle.ARG_PRODUCTO_NAME,producto.getNombre());

            Fragment productoDetalle = new ProductoDetalle();
            productoDetalle.setArguments(args);

            fragmentMng.beginTransaction()
                    .replace(R.id.content_frame, productoDetalle)
                    .addToBackStack(ProductoDetalle.FRAGMENT_NAME)
                    .commit();
        }
    };

    private class CargaProductosAsync extends AsyncTask<Void, Void, RowProductoAdapter> {
        @Override
        protected RowProductoAdapter doInBackground(Void... params) {
            ArrayList<RowProducto> values = new ProductoBO(ListadoProductos.this.getActivity()).getByCategoria(categoria);
            RowProductoAdapter adapter = new RowProductoAdapter(ListadoProductos.this.getActivity(), values);
            return adapter;
        }

        @Override
        protected void onPostExecute(RowProductoAdapter adapter) {
            if (adapter != null) {
                listView.setAdapter(adapter);
                listView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(ListadoProductos.this.getActivity(),
                    "Carga de datos cancelada...",
                    Toast.LENGTH_SHORT).show();
        }

    }

}
