package cphcarolina.comerua.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cphcarolina.comerua.R;
import cphcarolina.comerua.dao.CafeteriaDAO;
import cphcarolina.comerua.listHelpers.RowCafeteria;
import cphcarolina.comerua.listHelpers.RowCafeteriaAdapter;

public class ListadoCafeterias extends BaseFragment {
    private static final String TAG = "Activity ListadoCafeterias";
    public static final String FRAGMENT_NAME = "listadoCafeterias";

    protected ListView listView;
    protected ProgressBar progressBar;
    protected Spinner spinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        view = inflater.inflate(R.layout.fragment_listado_cafeterias,container,false);

        TextView title = (TextView) findViewById(R.id.textCafeteriaTitle);
        title.setText("Cafeterías");
        title.setTextColor(getResources().getColor(R.color.black));


        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.cafeterias_order,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setGravity(Gravity.RIGHT);
        spinner.setAdapter(adapter);
        /*spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                new CargaCafeteriasAsync().execute();
            }
        });*/

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(listListener);

        new CargaCafeteriasAsync().execute();

        return view;
    }
/*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_listado, menu);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }
*/
    private AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            new CargaCafeteriasAsync().execute();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemClickListener listListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG,"listListener pressed");
            RowCafeteria row = (RowCafeteria) parent.getItemAtPosition(position);
            int index = new CafeteriaDAO(ListadoCafeterias.this.getActivity()).getID(row.getNombre());

            Bundle args = new Bundle();
            args.putInt(ARG_CAFETERIA_INDEX,index);
            Log.d(TAG,"El INDEX ES "+index);

            Fragment cafeteriaPrincipal = new CafeteriaPrincipal();
            cafeteriaPrincipal.setArguments(args);

            fragmentMng.beginTransaction()
                    .replace(R.id.content_frame, cafeteriaPrincipal)
                    .addToBackStack("cafeteriaPrincipal")
                    .commit();
        }
    };

    private class CargaCafeteriasAsync extends AsyncTask<Void, Void, RowCafeteriaAdapter> {

        @Override
        protected RowCafeteriaAdapter doInBackground(Void... params) {
            ArrayList<RowCafeteria> values = null;

            switch (spinner.getSelectedItemPosition()) {
                case 0:
                    Log.d(TAG,"list sort by AZ");
                    values = new CafeteriaDAO(ListadoCafeterias.this.getActivity()).listCafeteriasAZ();
                    break;
                case 1:
                    Log.d(TAG,"list sort by distance");
                    values = new CafeteriaDAO(ListadoCafeterias.this.getActivity()).listCafeteriasDistancia();
                    break;
                case 2:
                    Log.d(TAG,"list sort by value");
                    values = new CafeteriaDAO(ListadoCafeterias.this.getActivity()).listCafeteriasValoracion();
                    break;
            }

            RowCafeteriaAdapter adapter = new RowCafeteriaAdapter(ListadoCafeterias.this.getActivity(), values);
            return adapter;
        }

        @Override
        protected void onPostExecute(RowCafeteriaAdapter adapter) {
            if (adapter != null) {
                listView.setAdapter(adapter);
                listView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(ListadoCafeterias.this.getActivity(),
                    "Carga de datos cancelada...",
                    Toast.LENGTH_SHORT).show();
        }
    }

}
