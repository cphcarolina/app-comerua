package cphcarolina.comerua.fragments;

import android.app.ActionBar;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cphcarolina.comerua.R;
import cphcarolina.comerua.bo.HorarioBO;
import cphcarolina.comerua.dao.CafeteriaDAO;
import cphcarolina.comerua.dao.ComentarioDAO;
import cphcarolina.comerua.listHelpers.RowComentario;
import cphcarolina.comerua.listHelpers.RowComentarioAdapter;

public class CafeteriaPrincipal extends BaseFragment {
    private static final String TAG = "Activity CafeteriaPrincipal";
    public static final String FRAGMENT_NAME = "cafeteriaPrincipal";
    ProgressBar progressBar;
    LinearLayout buttons;
    private  View panelHorarios, panelInformacion, panelContacto, panelMapa;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        view = inflater.inflate(R.layout.fragment_cafeteria_principal,container, false);
        cargarPaneles();

        TextView title = (TextView) findViewById(R.id.textCafeteriaTitle);
        if(cafeteria!=null) {
            cargarValoraciones();
            title.setText(cafeteria.getNombre());
            createImageButtons();
            buttons = (LinearLayout) findViewById(R.id.btnsLayout);
            createComments();

            cafeteria = new CafeteriaDAO(this.getActivity()).getInfo(cafeteria);

            printHorarios();
            printInstalaciones();
            printContacto();
            printMapa();
/*
            ImageButton info = (ImageButton) findViewById(R.id.infoButton);
            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "button info pressed");
                    Bundle args = new Bundle();
                    args.putInt(ARG_CAFETERIA_INDEX,indiceCafeteria);

                    Fragment infoCafeteria = new InfoCafeteria();
                    infoCafeteria.setArguments(args);

                    fragmentMng.beginTransaction()
                            .replace(R.id.content_frame, infoCafeteria)
                            .addToBackStack(InfoCafeteria.FRAGMENT_NAME)
                            .commit();
                }
            });*/
        } else {
            title.setText("Cafetería no cargada");
        }
        return view;
    }

    private void createImageButtons() {

        setPpialImageButton(R.id.imgBtnCalientes, R.drawable.cat_calientes,
                getString(R.string.iconCalientes),
                getString(R.string.catBebidasCalientes));

        setPpialImageButton(R.id.imgBtnAlcohol, R.drawable.cat_alcohol,
                getString(R.string.iconAlcoholicas),
                getString(R.string.catBebidasAlcoholicas));

        setPpialImageButton(R.id.imgBtnAgua, R.drawable.cat_agua,
                getString(R.string.iconAguas),
                getString(R.string.catAguas));

        setPpialImageButton(R.id.imgBtnBolleria, R.drawable.cat_bollos,
                getString(R.string.iconBollos),
                getString(R.string.catBollos));

        setPpialImageButton(R.id.imgBtnTapas, R.drawable.cat_tapas,
                getString(R.string.iconTapas),
                getString(R.string.catTapas));

        setPpialImageButton(R.id.imgBtnEnsaladas, R.drawable.cat_ensalada,
                getString(R.string.iconSalad),
                getString(R.string.catSalad));

        setPpialImageButton(R.id.imgBtnCombi, R.drawable.cat_combi,
                getString(R.string.iconCombi),
                getString(R.string.catCombi));

        setPpialImageButton(R.id.imgBtnMenu, R.drawable.cat_menu,
                getString(R.string.iconMenu),
                getString(R.string.catMenu));

        setPpialImageButton(R.id.imgBtnBocatas, R.drawable.cat_bocatas,
                getString(R.string.iconBocatas),
                getString(R.string.catBocatas));

    }

    private void createComments() {
        ArrayList<RowComentario> values = new ComentarioDAO(this.getActivity()).getPrimeros(indiceCafeteria);
        RowComentarioAdapter adapter = new RowComentarioAdapter(this.getActivity(), values);

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        TextView moreComments = (TextView) findViewById(R.id.moreComments);
        moreComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"button moreComments pressed");
                Bundle args = new Bundle();
                args.putInt(ARG_CAFETERIA_INDEX,indiceCafeteria);

                Fragment listadoComentarios = new ListadoComentarios();
                listadoComentarios.setArguments(args);

                fragmentMng.beginTransaction()
                        .replace(R.id.content_frame, listadoComentarios)
                        .addToBackStack(ListadoComentarios.FRAGMENT_NAME)
                        .commit();
            }
        });
    }

    private class CargaIconsAsync extends AsyncTask<Void, Void, LinearLayout> {
        @Override
        protected LinearLayout doInBackground(Void... voids) {
            setPpialImageButton(R.id.imgBtnCalientes, R.drawable.cat_calientes,
                    getString(R.string.iconCalientes),
                    getString(R.string.catBebidasCalientes));

            setPpialImageButton(R.id.imgBtnAlcohol, R.drawable.cat_alcohol,
                    getString(R.string.iconAlcoholicas),
                    getString(R.string.catBebidasAlcoholicas));

            setPpialImageButton(R.id.imgBtnAgua, R.drawable.cat_agua,
                    getString(R.string.iconAguas),
                    getString(R.string.catAguas));

            setPpialImageButton(R.id.imgBtnBolleria, R.drawable.cat_bollos,
                    getString(R.string.iconBollos),
                    getString(R.string.catBollos));

            setPpialImageButton(R.id.imgBtnTapas, R.drawable.cat_tapas,
                    getString(R.string.iconTapas),
                    getString(R.string.catTapas));

            setPpialImageButton(R.id.imgBtnEnsaladas, R.drawable.cat_ensalada,
                    getString(R.string.iconSalad),
                    getString(R.string.catSalad));

            setPpialImageButton(R.id.imgBtnCombi, R.drawable.cat_combi,
                    getString(R.string.iconCombi),
                    getString(R.string.catCombi));

            setPpialImageButton(R.id.imgBtnMenu, R.drawable.cat_menu,
                    getString(R.string.iconMenu),
                    getString(R.string.catMenu));

            setPpialImageButton(R.id.imgBtnBocatas, R.drawable.cat_bocatas,
                    getString(R.string.iconBocatas),
                    getString(R.string.catBocatas));

            return buttons;
        }

        @Override
        protected void onPostExecute(LinearLayout layout) {
            if (layout != null) {
                layout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(CafeteriaPrincipal.this.getActivity(),
                    "Carga de datos cancelada...",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private RelativeLayout setPpialImageButton(int id, int img, String texto, final String categoria) {
        RelativeLayout layout = (RelativeLayout) findViewById(id);

        TextView txt = (TextView) layout.getChildAt(1);
        txt.setText(texto);

        ImageButton btn = (ImageButton) layout.getChildAt(0);
        btn.setImageResource(img);
        btn.setBackgroundResource(button);
        if(categoria!=null) {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "button " + categoria + " pressed");
                    Bundle args = new Bundle();
                    args.putInt(ARG_CAFETERIA_INDEX, indiceCafeteria);
                    args.putString(ListadoProductos.ARG_CATEGORIA_NAME, categoria);

                    Fragment listadoProductos = new ListadoProductos();
                    listadoProductos.setArguments(args);
                    fragmentMng.beginTransaction()
                            .replace(R.id.content_frame, listadoProductos)
                            .addToBackStack(ListadoProductos.FRAGMENT_NAME+categoria)
                            .commit();
                }
            });
        }
        return layout;
    }


    private void cargarPaneles() {
        // Reestablecer paneles
        panelHorarios = findViewById(R.id.panelHorarios);
        panelInformacion = findViewById(R.id.panelInformacion);
        panelContacto = findViewById(R.id.panelContacto);
        panelMapa = findViewById(R.id.panelMapa);
        panelHorarios.setVisibility(View.GONE);
        panelInformacion.setVisibility(View.GONE);
        panelContacto.setVisibility(View.GONE);
        panelMapa.setVisibility(View.GONE);

        setInfoImageButton(R.id.imgBtnHorario, R.drawable.ic_action_go_to_today, "Horarios");
        setInfoImageButton(R.id.imgBtnInstalaciones, R.drawable.ic_action_about, "Info");
        setInfoImageButton(R.id.imgBtnContacto, R.drawable.ic_action_call, "Contacto");
        setInfoImageButton(R.id.imgBtnMapa, R.drawable.ic_action_map, "Mapa");
    }

    protected RelativeLayout setInfoImageButton(int id, int img, final String texto) {
        RelativeLayout layout = (RelativeLayout) findViewById(id);

        TextView txt = (TextView) layout.getChildAt(1);
        txt.setText(texto);

        ImageButton btn = (ImageButton) layout.getChildAt(0);
        btn.setImageResource(img);
        btn.setBackgroundResource(button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (texto) {
                    case "Horarios":
                        if(panelHorarios.getVisibility() == View.GONE) {
                            Log.d(TAG, "Horario pressed and GONE->VISIBLE");
                            panelHorarios.setVisibility(View.VISIBLE);
                            panelInformacion.setVisibility(View.GONE);
                            panelContacto.setVisibility(View.GONE);
                            panelMapa.setVisibility(View.GONE);
                        } else {
                            Log.d(TAG,"Horario pressed and VISIBLE->GONE");
                            panelHorarios.setVisibility(View.GONE);
                        }
                        break;
                    case "Info":
                        if(panelInformacion.getVisibility() == View.GONE) {
                            Log.d(TAG,"Informacion pressed and GONE->VISIBLE");
                            panelHorarios.setVisibility(View.GONE);
                            panelInformacion.setVisibility(View.VISIBLE);
                            panelContacto.setVisibility(View.GONE);
                            panelMapa.setVisibility(View.GONE);
                        } else {
                            Log.d(TAG,"Informacion pressed and VISIBLE->GONE");
                            panelInformacion.setVisibility(View.GONE);
                        }
                        break;
                    case "Contacto":
                        if(panelContacto.getVisibility() == View.GONE) {
                            Log.d(TAG,"Contacto pressed and GONE->VISIBLE");
                            panelHorarios.setVisibility(View.GONE);
                            panelInformacion.setVisibility(View.GONE);
                            panelContacto.setVisibility(View.VISIBLE);
                            panelMapa.setVisibility(View.GONE);
                        } else {
                            Log.d(TAG,"Contacto pressed and VISIBLE->GONE");
                            panelContacto.setVisibility(View.GONE);
                        }
                        break;
                    case "Mapa":
                        if(panelMapa.getVisibility() == View.GONE) {
                            Log.d(TAG, "Mapa pressed and GONE->VISIBLE");
                            panelHorarios.setVisibility(View.GONE);
                            panelInformacion.setVisibility(View.GONE);
                            panelContacto.setVisibility(View.GONE);
                            panelMapa.setVisibility(View.VISIBLE);
                        } else {
                            Log.d(TAG,"Mapa pressed and VISIBLE->GONE");
                            panelMapa.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        });

        return layout;
    }


    private void printHorarios() {
        ArrayList<HorarioBO> horarios = cafeteria.getHorarios();
        LinearLayout layout = (LinearLayout) findViewById(R.id.panelHorarios);

        if(horarios != null) {
            for (HorarioBO bo : horarios) {
                LinearLayout conjunto = new LinearLayout(CafeteriaPrincipal.this.getActivity());
                conjunto.setOrientation(LinearLayout.VERTICAL);

                TextView titulo = (TextView) getTemplate(CafeteriaPrincipal.this.getActivity(), R.layout.inc_text_title);
                titulo.setText(bo.getNombre());
                conjunto.addView(titulo);

                TableLayout table = new TableLayout(CafeteriaPrincipal.this.getActivity());
                table.setPadding(20, 0, 0, 0);
                HashMap<String, String> semana = bo.getHorario();
                Iterator<?> it = semana.entrySet().iterator();


                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    TableRow row = new TableRow(this.getActivity());

                    TextView key = new TextView(this.getActivity());
                    key.setText(pair.getKey().toString() + ": ");


                    TextView value = new TextView(this.getActivity());
                    value.setText(pair.getValue().toString());

                    it.remove(); // avoids a ConcurrentModificationException

                    row.addView(key, 0);
                    row.addView(value, 1);

                    table.addView(row, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                }
                conjunto.addView(table);
                layout.addView(conjunto);
            }
        } else {
            TextView msg = new TextView(this.getActivity());
            msg.setText("Horarios no disponibles");
            layout.addView(msg);
        }

    }

    private void printInstalaciones() {
        TextView textAforo = (TextView) findViewById(R.id.textAforo);
        if (cafeteria.getAforo() == -1) textAforo.setText(" - ");
        else textAforo.setText(String.valueOf(cafeteria.getAforo()));

        TextView textBillar = (TextView) findViewById(R.id.textBillar);
        if (cafeteria.getBillar() == -1) textBillar.setText(" - ");
        else textBillar.setText(String.valueOf(cafeteria.getBillar()));

        TextView textFutbolin = (TextView) findViewById(R.id.textFutbolin);
        if (cafeteria.getFutbolin() == -1) textFutbolin.setText(" - ");
        else textFutbolin.setText(String.valueOf(cafeteria.getFutbolin()));

        TextView textMicroondas = (TextView) findViewById(R.id.textMicroondas);
        if(cafeteria.getMicroondas() == -1) textMicroondas.setText(" - ");
        else textMicroondas.setText((String.valueOf(cafeteria.getMicroondas())));
    }

    private void printContacto() {
        TextView textGerente = (TextView) findViewById(R.id.textGerente);
        textGerente.setText(cafeteria.getResponsable());

        View panel = findViewById(R.id.panelContTlf);
        if (cafeteria.getTelefonos() != null) {
            crearTelefonos(cafeteria.getTelefonos());
        } else {
            panel.setVisibility(View.GONE);
        }

        panel = findViewById(R.id.panelContEmail);
        if (cafeteria.getEmails() != null) {
            crearEmails(cafeteria.getEmails());
        } else {
            panel.setVisibility(View.GONE);
        }
    }

    private void crearTelefonos(Map<String, Integer> telefonos) {
        Iterator<?> it = telefonos.entrySet().iterator();
        TableLayout table = (TableLayout) findViewById(R.id.tableTlf);

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            TableRow row = new TableRow(this.getActivity());

            TextView key = new TextView(this.getActivity());
            key.setText(pair.getKey().toString() + ": ");


            TextView value = new TextView(this.getActivity());
            value.setText(pair.getValue().toString());
            value.setAutoLinkMask(Linkify.PHONE_NUMBERS);

            it.remove(); // avoids a ConcurrentModificationException

            row.addView(key, 0);
            row.addView(value, 1);

            table.addView(row, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
    }

    private void crearEmails(Map<String, String> emails) {
        Iterator<?> it = emails.entrySet().iterator();
        TableLayout table = (TableLayout) findViewById(R.id.tableEmails);

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            TableRow row = new TableRow(this.getActivity());

            TextView key = new TextView(this.getActivity());
            key.setText(pair.getKey().toString() + ": ");

            TextView value = new TextView(this.getActivity());
            value.setText(pair.getValue().toString());
            value.setAutoLinkMask(Linkify.EMAIL_ADDRESSES);

            it.remove(); // avoids a ConcurrentModificationException

            row.addView(key, 0);
            row.addView(value, 1);

            table.addView(row, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
    }

    private void printMapa() {
        ImageView mapa = (ImageView) findViewById(R.id.mapa);
        switch (indiceCafeteria) {
            case 0:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_cs1));
                break;
            case 1:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_cs2));
                break;
            case 2:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_cs3));
                break;
            case 3:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_eps));
                break;
            case 4:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_don_jamon));
                break;
            case 5:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_ciencias));
                break;
        }
    }
}
