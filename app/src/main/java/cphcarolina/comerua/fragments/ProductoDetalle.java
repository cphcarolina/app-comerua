package cphcarolina.comerua.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import cphcarolina.comerua.R;
import cphcarolina.comerua.dao.ProductoDAO;
import cphcarolina.comerua.vo.ProductoVO;


public class ProductoDetalle extends BaseFragment {
    public static final String ARG_PRODUCTO_NAME = "productoName";
    private String TAG = "Activity ProductoDetalle";
    public static final String FRAGMENT_NAME = "productoDetalle";
    private String nombreProducto;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        view = inflater.inflate(R.layout.fragment_producto_detalle, container, false);
        nombreProducto = getArguments().getString(ARG_PRODUCTO_NAME,null);

        TextView title = (TextView) findViewById(R.id.textCafeteriaTitle);
        if(nombreProducto!=null) {
            ProductoVO producto = new ProductoDAO(this.getActivity()).getByName(nombreProducto);
            title.setText(producto.getNombre());

            // Valoraciones
            TextView textGlobal = (TextView) findViewById(R.id.puntuacionInfoGlobal);
            TextView textDiario = (TextView) findViewById(R.id.puntuacionInfoDiario);
            if (textGlobal != null) textGlobal.setText("(" + producto.getVotosGlobal() + " votos)");
            if (textDiario != null) textDiario.setText("(" + producto.getVotosDiario() + " votos)");

            RatingBar global = (RatingBar) findViewById(R.id.ratingInfoGlobal);
            RatingBar diario = (RatingBar) findViewById(R.id.ratingInfoDiario);
            if (global != null) global.setRating(producto.getPuntuacionGlobal());
            if (diario != null) diario.setRating(producto.getPuntuacionDiario());

            // Precio
            TextView precio = (TextView) findViewById(R.id.textPrecio);
            if(precio != null) precio.setText(producto.getstrPrecio());

            // Descripción
            TextView desc = (TextView) findViewById(R.id.textDesc);
            if(desc != null) desc.setText(producto.getDescripcion());
        } else {
            title.setText("Producto no cargado");
        }

        return view;
    }

}
