package cphcarolina.comerua.fragments;


import android.app.ActionBar;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cphcarolina.comerua.R;
import cphcarolina.comerua.bo.HorarioBO;
import cphcarolina.comerua.dao.CafeteriaDAO;

/**
 * Created by Carolina on 24/02/2015.
 */
public class InfoCafeteria extends BaseFragment {
    private static final String TAG = "Activity InfoCafeteria";
    public static final String FRAGMENT_NAME = "infoCafeteria";

    private  View panelHorarios, panelInformacion, panelContacto, panelMapa;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        view = inflater.inflate(R.layout.fragment_cafeteria_info, container, false);
        cargarPaneles();

        TextView title = (TextView) findViewById(R.id.textCafeteriaTitle);
        title.setText("Información");

        if(cafeteria!=null) {
            cargarValoraciones();
            cafeteria = new CafeteriaDAO(this.getActivity()).getInfo(cafeteria);

            printHorarios();
            printInstalaciones();
            printContacto();
            printMapa();
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    private void cargarPaneles() {
        // Reestablecer paneles
        panelHorarios = findViewById(R.id.panelHorarios);
        panelInformacion = findViewById(R.id.panelInformacion);
        panelContacto = findViewById(R.id.panelContacto);
        panelMapa = findViewById(R.id.panelMapa);
        panelHorarios.setVisibility(View.GONE);
        panelInformacion.setVisibility(View.GONE);
        panelContacto.setVisibility(View.GONE);
        panelMapa.setVisibility(View.GONE);

        setInfoImageButton(R.id.imgBtnHorario, R.drawable.ic_action_go_to_today, "Horarios");
        setInfoImageButton(R.id.imgBtnInstalaciones, R.drawable.ic_action_about, "Info");
        setInfoImageButton(R.id.imgBtnContacto, R.drawable.ic_action_call, "Contacto");
        setInfoImageButton(R.id.imgBtnMapa, R.drawable.ic_action_map, "Mapa");
    }

    protected RelativeLayout setInfoImageButton(int id, int img, final String texto) {
        RelativeLayout layout = (RelativeLayout) findViewById(id);

        TextView txt = (TextView) layout.getChildAt(1);
        txt.setText(texto);

        ImageButton btn = (ImageButton) layout.getChildAt(0);
        btn.setImageResource(img);
        btn.setBackgroundResource(button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (texto) {
                    case "Horarios":
                        if(panelHorarios.getVisibility() == View.GONE) {
                            Log.d(TAG, "Horario pressed and GONE->VISIBLE");
                            panelHorarios.setVisibility(View.VISIBLE);
                            panelInformacion.setVisibility(View.GONE);
                            panelContacto.setVisibility(View.GONE);
                            panelMapa.setVisibility(View.GONE);
                        } else {
                            Log.d(TAG,"Horario pressed and VISIBLE->GONE");
                            panelHorarios.setVisibility(View.GONE);
                        }
                        break;
                    case "Info":
                        if(panelInformacion.getVisibility() == View.GONE) {
                            Log.d(TAG,"Informacion pressed and GONE->VISIBLE");
                            panelHorarios.setVisibility(View.GONE);
                            panelInformacion.setVisibility(View.VISIBLE);
                            panelContacto.setVisibility(View.GONE);
                            panelMapa.setVisibility(View.GONE);
                        } else {
                            Log.d(TAG,"Informacion pressed and VISIBLE->GONE");
                            panelInformacion.setVisibility(View.GONE);
                        }
                        break;
                    case "Contacto":
                        if(panelContacto.getVisibility() == View.GONE) {
                            Log.d(TAG,"Contacto pressed and GONE->VISIBLE");
                            panelHorarios.setVisibility(View.GONE);
                            panelInformacion.setVisibility(View.GONE);
                            panelContacto.setVisibility(View.VISIBLE);
                            panelMapa.setVisibility(View.GONE);
                        } else {
                            Log.d(TAG,"Contacto pressed and VISIBLE->GONE");
                            panelContacto.setVisibility(View.GONE);
                        }
                        break;
                    case "Mapa":
                        if(panelMapa.getVisibility() == View.GONE) {
                            Log.d(TAG, "Mapa pressed and GONE->VISIBLE");
                            panelHorarios.setVisibility(View.GONE);
                            panelInformacion.setVisibility(View.GONE);
                            panelContacto.setVisibility(View.GONE);
                            panelMapa.setVisibility(View.VISIBLE);
                        } else {
                            Log.d(TAG,"Mapa pressed and VISIBLE->GONE");
                            panelMapa.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        });

        return layout;
    }


    private void printHorarios() {
        ArrayList<HorarioBO> horarios = cafeteria.getHorarios();
        LinearLayout layout = (LinearLayout) findViewById(R.id.panelHorarios);

        if(horarios != null) {
            for (HorarioBO bo : horarios) {
                LinearLayout conjunto = new LinearLayout(InfoCafeteria.this.getActivity());
                conjunto.setOrientation(LinearLayout.VERTICAL);

                TextView titulo = (TextView) getTemplate(InfoCafeteria.this.getActivity(), R.layout.inc_text_title);
                titulo.setText(bo.getNombre());
                conjunto.addView(titulo);

                TableLayout table = new TableLayout(InfoCafeteria.this.getActivity());
                table.setPadding(20, 0, 0, 0);
                HashMap<String, String> semana = bo.getHorario();
                Iterator<?> it = semana.entrySet().iterator();


                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    TableRow row = new TableRow(this.getActivity());

                    TextView key = new TextView(this.getActivity());
                    key.setText(pair.getKey().toString() + ": ");


                    TextView value = new TextView(this.getActivity());
                    value.setText(pair.getValue().toString());

                    it.remove(); // avoids a ConcurrentModificationException

                    row.addView(key, 0);
                    row.addView(value, 1);

                    table.addView(row, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                }
                conjunto.addView(table);
                layout.addView(conjunto);
            }
        }

    }

    private void printInstalaciones() {
        TextView textAforo = (TextView) findViewById(R.id.textAforo);
        TextView textBillar = (TextView) findViewById(R.id.textBillar);
        TextView textFutbolin = (TextView) findViewById(R.id.textFutbolin);
        if (cafeteria.getAforo() == -1) textAforo.setText(" - ");
        else textAforo.setText(String.valueOf(cafeteria.getAforo()));
        if (cafeteria.getBillar() == -1) textBillar.setText(" - ");
        else textBillar.setText(String.valueOf(cafeteria.getBillar()));
        if (cafeteria.getFutbolin() == -1) textFutbolin.setText(" - ");
        else textFutbolin.setText(String.valueOf(cafeteria.getFutbolin()));
    }

    private void printContacto() {
        TextView textGerente = (TextView) findViewById(R.id.textGerente);
        textGerente.setText(cafeteria.getResponsable());

        View panel = findViewById(R.id.panelContTlf);
        if (cafeteria.getTelefonos() != null) {
            crearTelefonos(cafeteria.getTelefonos());
        } else {
            panel.setVisibility(View.GONE);
        }

        panel = findViewById(R.id.panelContEmail);
        if (cafeteria.getEmails() != null) {
            crearEmails(cafeteria.getEmails());
        } else {
            panel.setVisibility(View.GONE);
        }
    }

    private void crearTelefonos(Map<String, Integer> telefonos) {
        Iterator<?> it = telefonos.entrySet().iterator();
        TableLayout table = (TableLayout) findViewById(R.id.tableTlf);

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            TableRow row = new TableRow(this.getActivity());

            TextView key = new TextView(this.getActivity());
            key.setText(pair.getKey().toString() + ": ");


            TextView value = new TextView(this.getActivity());
            value.setText(pair.getValue().toString());
            value.setAutoLinkMask(Linkify.PHONE_NUMBERS);

            it.remove(); // avoids a ConcurrentModificationException

            row.addView(key, 0);
            row.addView(value, 1);

            table.addView(row, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
    }

    private void crearEmails(Map<String, String> emails) {
        Iterator<?> it = emails.entrySet().iterator();
        TableLayout table = (TableLayout) findViewById(R.id.tableEmails);

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            TableRow row = new TableRow(this.getActivity());

            TextView key = new TextView(this.getActivity());
            key.setText(pair.getKey().toString() + ": ");

            TextView value = new TextView(this.getActivity());
            value.setText(pair.getValue().toString());
            value.setAutoLinkMask(Linkify.EMAIL_ADDRESSES);

            it.remove(); // avoids a ConcurrentModificationException

            row.addView(key, 0);
            row.addView(value, 1);

            table.addView(row, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
    }

    private void printMapa() {
        ImageView mapa = (ImageView) findViewById(R.id.mapa);
        switch (indiceCafeteria) {
            case 0:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_cs1));
                break;
            case 1:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_cs2));
                break;
            case 2:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_cs3));
                break;
            case 3:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_eps));
                break;
            case 4:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_don_jamon));
                break;
            case 5:
                mapa.setBackground(getResources().getDrawable(R.drawable.mapa_ciencias));
                break;
        }
    }
}
