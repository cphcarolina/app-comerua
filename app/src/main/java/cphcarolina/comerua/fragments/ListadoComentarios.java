package cphcarolina.comerua.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cphcarolina.comerua.R;
import cphcarolina.comerua.dao.ComentarioDAO;
import cphcarolina.comerua.listHelpers.RowComentario;
import cphcarolina.comerua.listHelpers.RowComentarioAdapter;


public class ListadoComentarios extends BaseFragment {
    private static final String TAG = "Activity ListadoComentarios";
    public static final String FRAGMENT_NAME = "listadoComentarios";
    ListView listView;
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        view = inflater.inflate(R.layout.fragment_listado_comentarios, container, false);

        TextView title = (TextView) findViewById(R.id.textCafeteriaTitle);
        title.setText("Comentarios");
        if(cafeteria!=null) {
            progressBar = (ProgressBar) findViewById(R.id.progressBar);
            listView = (ListView) findViewById(R.id.listView);
            new CargaComentariosAsync().execute();
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    private class CargaComentariosAsync extends AsyncTask<Void, Void, RowComentarioAdapter> {
        @Override
        protected RowComentarioAdapter doInBackground(Void... params) {
            ArrayList<RowComentario> values = new ComentarioDAO(ListadoComentarios.this.getActivity()).getComentarios(indiceCafeteria);
            RowComentarioAdapter adapter = new RowComentarioAdapter(ListadoComentarios.this.getActivity(), values);
            return adapter;
        }

        @Override
        protected void onPostExecute(RowComentarioAdapter adapter) {
            if (adapter != null) {
                listView.setAdapter(adapter);
                listView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(ListadoComentarios.this.getActivity(),
                    "Carga de datos cancelada...",
                    Toast.LENGTH_SHORT).show();
        }
    }

}