package cphcarolina.comerua.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.usage.UsageEvents;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import cphcarolina.comerua.MainActivity;
import cphcarolina.comerua.R;
import cphcarolina.comerua.dao.CafeteriaDAO;
import cphcarolina.comerua.vo.CafeteriaVO;

/**
 * Fragment en el que se basan el resto de fragments
 * Created by Carolina on 19/02/2015.
 */
public class BaseFragment extends Fragment {
    private static final String TAG = "BaseFragment";
    public static final String ARG_CAFETERIA_INDEX = "cafeteriaID";

    protected View view;
    protected FragmentManager fragmentMng;

    protected int indiceCafeteria;
    protected int color, backColor, button;
    protected CafeteriaVO cafeteria;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();

        if(bundle!=null) {
            indiceCafeteria = bundle.getInt(ARG_CAFETERIA_INDEX, -1);
            if (indiceCafeteria != -1) {
                cafeteria = new CafeteriaDAO(this.getActivity()).getCafeteria(indiceCafeteria);
                Log.d(TAG, "Cafetería "+cafeteria.getNombre()+" found");
                ((MainActivity) getActivity()).setTitle(cafeteria.getNombre());
                changeColor();
            } else {
                cafeteria = null;
                Log.d(TAG, "There is no cafeteria");
            }
        }
        else {
            Log.d(TAG, "There is no bundle.");
        }
        fragmentMng = getFragmentManager();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RelativeLayout cabecera = (RelativeLayout) findViewById(R.id.cabeceraLayout);
        if(cabecera != null) cabecera.setBackgroundResource(color);
        LinearLayout comentarios = (LinearLayout) findViewById(R.id.comentariosTitleLayout);
        if(comentarios != null) comentarios.setBackgroundResource(color);
        LinearLayout informacion = (LinearLayout) findViewById(R.id.informacionTitleLayout);
        if(informacion != null) informacion.setBackgroundResource(color);
        View div = findViewById(R.id.lineaDiv);
        if(div != null) div.setBackgroundResource(backColor);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_principal,menu);
    }

    public void cargarValoraciones() {
        if(indiceCafeteria!=1) {
            TextView textView = (TextView) findViewById(R.id.textCafeteriaTitle);
            if (textView != null) textView.setText(cafeteria.getNombre());
            int tGlobal = cafeteria.getVotosGlobal(), tDiario = cafeteria.getVotosDiario();
            float pGlobal = cafeteria.getPuntuacionGlobal(), pDiario = cafeteria.getPuntuacionDiario();
            // Cabecera
            TextView textGlobal = (TextView) findViewById(R.id.puntuacionGlobal);
            TextView textDiario = (TextView) findViewById(R.id.puntuacionDiario);
            if (textGlobal != null) textGlobal.setText("(" + tGlobal + " votos)");
            if (textDiario != null) textDiario.setText("(" + tDiario + " votos)");

            RatingBar global = (RatingBar) findViewById(R.id.ratingGlobal);
            final RatingBar diario = (RatingBar) findViewById(R.id.ratingDiario);
            if (global != null) global.setRating(pGlobal);
            if (diario != null) {
                diario.setRating(pDiario);
                diario.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        boolean ok = false;
                        if(event.getAction() == MotionEvent.ACTION_UP) {
                            Toast.makeText(BaseFragment.this.getActivity(), "Has votado " + diario.getRating(), Toast.LENGTH_SHORT).show();
                            cafeteria.addVoto(diario.getRating());
                            cargarValoraciones();
                        }
                        return ok;
                    }
                });
            }

            // Información
            TextView textInfoGlobal = (TextView) findViewById(R.id.puntuacionInfoGlobal);
            TextView textInfoDiario = (TextView) findViewById(R.id.puntuacionInfoDiario);
            if (textInfoGlobal != null) textInfoGlobal.setText("(" + tGlobal + " votos)");
            if (textInfoDiario != null) textInfoDiario.setText("(" + tDiario + " votos)");


            RatingBar globalInfo = (RatingBar) findViewById(R.id.ratingInfoGlobal);
            final RatingBar diarioInfo = (RatingBar) findViewById(R.id.ratingInfoDiario);
            if (globalInfo != null) globalInfo.setRating(pGlobal);
            if (diarioInfo != null) {
                diarioInfo.setRating(pDiario);
                diarioInfo.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        boolean ok = false;
                        if(event.getAction() == MotionEvent.ACTION_UP) {
                            Toast.makeText(BaseFragment.this.getActivity(), "Has votado " + diarioInfo.getRating(), Toast.LENGTH_SHORT).show();
                            cafeteria.addVoto(diarioInfo.getRating());
                            cargarValoraciones();
                        }
                        return ok;
                    }
                });
            }
        }
    }

    protected void changeColor() {
        switch (indiceCafeteria) {
            case 0:
                color = R.color.cafeteria1;
                backColor = R.color.cafeteria1_2;
                button = R.drawable.custom_button1;
                break;
            case 1:
                color = R.color.cafeteria2;
                backColor = R.color.cafeteria2_2;
                button = R.drawable.custom_button2;
                break;
            case 2:
                color = R.color.cafeteria3;
                backColor = R.color.cafeteria3_2;
                button = R.drawable.custom_button3;
                break;
            case 3:
                color = R.color.cafeteria4;
                backColor = R.color.cafeteria4_2;
                button = R.drawable.custom_button4;
                break;
            case 4:
                color = R.color.cafeteria5;
                backColor = R.color.cafeteria5_2;
                button = R.drawable.custom_button5;
                break;
            case 5:
                color = R.color.cafeteria6;
                backColor = R.color.cafeteria6_2;
                button = R.drawable.custom_button6;
                break;
            case 6:
                color = R.color.cafeteria7;
                backColor = R.color.cafeteria7_2;
                button = R.drawable.custom_button7;
                break;
            default:
                color = Color.WHITE;
                backColor = R.color.fondo;
                button = R.drawable.custom_button;
                break;
        }
    }

    protected View findViewById(int id) {
        return view.findViewById(id);
    }

    protected View getTemplate(Activity activity, int id) {
        return activity.getLayoutInflater().inflate(id, null);
    }

}
